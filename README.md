# tweetsentiment

In order to properly run the application, the folder "dm_project" must be placed in the "Documents" folder of the user home directory. 
The code was compiled using Java 8, and it can be deployed either in Tomcat 8 or Glassifish 5.
------------
The main class, where all the text mining logic is implemented is:
it.unipi.iet.dm.sentiweb.admin.core.ClassifierBuilder
