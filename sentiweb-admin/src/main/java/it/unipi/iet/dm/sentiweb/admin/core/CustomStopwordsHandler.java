/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.iet.dm.sentiweb.admin.core;

import java.io.InputStream;
import java.io.Serializable;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import weka.core.stopwords.StopwordsHandler;


public class CustomStopwordsHandler implements StopwordsHandler, Serializable{
    
    private List<String> stopWords = new ArrayList<String>();
    
    public CustomStopwordsHandler(InputStream stopWordsIS, String separator){
        Scanner scanner = new Scanner(stopWordsIS, "UTF-8");
        while(scanner.hasNextLine()){
            String newLine = scanner.nextLine();
            if (newLine != null && !newLine.isEmpty()){
                newLine = Normalizer.normalize(newLine, Normalizer.Form.NFD); // translate UTF-8 chars properly
                newLine = newLine.trim();
                String[] values = newLine.split(separator);
                if (values.length > 0){
                    String stopWord = values[0].trim();
                    if (stopWord != null && !stopWord.isEmpty()){
                        stopWords.add(stopWord.toLowerCase());
                    }
                }
            }
        }
    }

    @Override
    public boolean isStopword(String string) {
        return stopWords.contains(string);
    }
    
}
