package it.unipi.iet.dm.sentiweb.admin.core;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import org.apache.tika.language.LanguageIdentifier;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.SerializationHelper;
import weka.core.Utils;
import weka.core.converters.ArffSaver;
import weka.core.stemmers.SnowballStemmer;
import weka.core.stemmers.Stemmer;
import weka.core.stopwords.StopwordsHandler;
import weka.core.tokenizers.NGramTokenizer;
import weka.core.tokenizers.Tokenizer;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.Reorder;
import weka.filters.unsupervised.attribute.StringToWordVector;



public class ClassifierBuilder {

    private final String PROJECT_DATA;

    private static final String TR_POSITIVE = "Positive.csv";
    private static final String TR_NEGATIVE = "Negative.csv";
    private static final String TR_NEUTRAL = "Neutral.csv";
    private static final String SEPARATOR_REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
    private static final String ITALIAN_STOPWORDS = "/stop_words/stopwords-it.txt";
    private static final String LANGUAGE = "it";
    private static final int FOLDS = 10;
    private static String LAST_MODEL_PATH = null;
    private static FilteredClassifier lastClassifier;

    enum ClassifierEnum {
        /*SVM, J48, NAIVE_BAYES, */
        MULTINOMIAL_NAIVE_BAYES;
    }

    public ClassifierBuilder() throws Exception {
        PROJECT_DATA = getFilePath(System.getProperty("user.home"), "Documents", "dm_project");
        createTrainingSetEvents();
        buildClassifierEvents();
        lastClassifier = (FilteredClassifier) weka.core.SerializationHelper.read(LAST_MODEL_PATH);
    }



    private void buildClassifierEvents() throws Exception {
        final String TRAININGSET_DIRECTORY = getFilePath(PROJECT_DATA, "training_set");
        String modelFilePath = getFilePath(PROJECT_DATA, "models", "iussoli.model");
        File modelFile = new File(modelFilePath);
        if (!modelFile.exists()) {
            /* model construction */
            String arffFilePath = getFilePath(TRAININGSET_DIRECTORY, "training_set.arff");
            String statsFilePath = getFilePath(PROJECT_DATA, "stats", "iussoli_stats.txt");
            buildClassifier(arffFilePath, statsFilePath, modelFilePath);
        }
        LAST_MODEL_PATH = modelFilePath;
    }

    private FilteredClassifier buildClassifier(String arffFilePath, String statsFilePath, String modelFilePath) throws Exception {
        /* loading training set */
        BufferedReader reader = new BufferedReader(new FileReader(arffFilePath));
        Instances dataSet = new Instances(reader); // get data from somewhere
        dataSet.setClassIndex(dataSet.numAttributes() - 1);
        System.out.println("Num. instances training set: " + dataSet.numInstances());

        Filter filterStrToWordVector = createStrToWordVectorFilter();
        Filter attrSelFilter = createAttributeSelectionFilter();
        Reorder reorder = new Reorder();
        reorder.setAttributeIndices("2-last,1");

        Filter[] filters = new Filter[3];
        MultiFilter multiFilter = new MultiFilter();
        filters[0] = filterStrToWordVector;
        filters[1] = reorder;
        filters[2] = attrSelFilter;
        multiFilter.setFilters(filters);
        multiFilter.setInputFormat(dataSet);

        Instances data = null;
        FilteredClassifier classifier = null;
        for (ClassifierEnum classifierEnum : ClassifierEnum.values()) {
            Classifier classifierAlg = buildClassifier(classifierEnum);
            System.out.println("ClassifierAlg " + classifierAlg.getClass().getName());
            /* this writer is created to handle the stats */
            BufferedWriter writer = new BufferedWriter(new FileWriter(statsFilePath + classifierAlg.getClass().getName()));

            long seed = System.currentTimeMillis();
            // setting up train- and test-set
            Random rand = new Random(seed);

            data = new Instances(dataSet);
            data.randomize(rand);
            data.stratify(FOLDS);

            classifier = new FilteredClassifier();
            classifier.setFilter(multiFilter);
            classifier.setClassifier(classifierAlg);

            Classifier[] crossFoldClassifiers = AbstractClassifier.makeCopies(classifier, FOLDS);

            classifier.buildClassifier(data);

            for (int n = 0; n < FOLDS; n++) {
                Instances trainingSet = data.trainCV(FOLDS, n);
                Instances testSet = data.testCV(FOLDS, n);
                System.out.println("Training instances: " + trainingSet.numInstances());
                System.out.println("Test instances: " + testSet.numInstances());

                Classifier testClassifier = crossFoldClassifiers[n];
                testClassifier.buildClassifier(trainingSet);
                // create new Evaluation object and pass the schema of the dataset
                Evaluation eval = new Evaluation(trainingSet);
                // evaluate classifier on test-set
                eval.evaluateModel(testClassifier, testSet);
                // print some stats about the result:
                System.out.println(eval.toSummaryString());
                writer.write(eval.toSummaryString());
                writer.newLine();
                writer.newLine();
                // more details:
                System.out.println(eval.toClassDetailsString());
                writer.write(eval.toClassDetailsString());
                writer.newLine();
                writer.newLine();
                // print confusion matrix
                System.out.println(eval.toMatrixString());
                writer.write(eval.toMatrixString());
                writer.newLine();
                writer.newLine();
            }

            SerializationHelper.write(modelFilePath, classifier);
            writer.close();
        }
        return classifier;
    }

    public String classifyTweet(String text) throws Exception {
        /* we build an instance with it metadata */
        Attribute textAttribute = new Attribute("text", (List<String>) null);
        List<String> nominalValues = new ArrayList<String>();
        nominalValues.add("positive");
        nominalValues.add("negative");
        nominalValues.add("neutral");
        Attribute classAttribute = new Attribute("tweet_class", nominalValues);

        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        attributes.add(textAttribute);
        attributes.add(classAttribute);

        /* lets construct our training set with an initial capacity of 10 elements */
        int initialCapacity = 10;
        Instances data = new Instances("new_set", attributes, initialCapacity);
        /* last attribute index is the class (it starts from 0 to N - 1) */
        data.setClassIndex(data.numAttributes() - 1);

        Instance instance = new DenseInstance(2);
        instance.setValue(textAttribute, text);
        instance.setDataset(data);

        /* classifyInstance call */
        double result = lastClassifier.classifyInstance(instance);
        String predictedClass = data.classAttribute().value((int) result);
        return predictedClass;
    }

    private Filter createAttributeSelectionFilter() throws Exception {
        AttributeSelection attrSelFilter = new AttributeSelection();
        InfoGainAttributeEval evaluator = new InfoGainAttributeEval();
        Ranker search = new Ranker();
        search.setOptions("-T -1.7976931348623157E308 -N -1".split(" "));
        attrSelFilter.setEvaluator(evaluator);
        attrSelFilter.setSearch(search);

        return attrSelFilter;
    }

    private Tokenizer createTokenizer() {
        int minSize = 1;
        int maxSize = 3;
        String delimiters = null;
        NGramTokenizer tokenizer = new NGramTokenizer();
        if (delimiters != null) {
            tokenizer.setDelimiters(delimiters);
        }
        tokenizer.setNGramMaxSize(maxSize);
        tokenizer.setNGramMinSize(minSize);
        return tokenizer;
    }

    private StopwordsHandler createStopWordsHandler() {
        String delimiter = "\\|";
        InputStream is = ClassifierBuilder.class.getResourceAsStream(ITALIAN_STOPWORDS);
        return new CustomStopwordsHandler(is, delimiter);
    }

    private Stemmer createStemmer() {
        String language = "italian";
        return new SnowballStemmer(language);
    }

    private Filter createStrToWordVectorFilter() throws Exception {
        /* creating tokenizer, stopwordshandler and stemmer */
        Tokenizer tokenizer = createTokenizer();
        StopwordsHandler stopwordsHandler = createStopWordsHandler();
        Stemmer snowballStemmer = createStemmer();

        /* creating and configuring custom filter */
        CustomStringToWordVector filter = new CustomStringToWordVector(0);

        filter.setAttributeIndices("first");
        filter.setStemmer(snowballStemmer);
        filter.setTokenizer(tokenizer);
        filter.setStopwordsHandler(stopwordsHandler);
        /* FROM HERE IT IS REQUIRED TO REVIEW THE DOCUMENTATION TO KNOW WHAT EACH PARAMETERS IS USED FOR */
        filter.setWordsToKeep(10000);
        filter.setDoNotOperateOnPerClassBasis(false);
        filter.setLowerCaseTokens(true);
        filter.setSaveDictionaryInBinaryForm(false);
        filter.setIDFTransform(true); // we want IDF data, so: true
        filter.setTFTransform(false); // false, since we want pure TF (term frequency)
        filter.setOutputWordCounts(false);
        filter.setNormalizeDocLength(new SelectedTag(StringToWordVector.FILTER_NORMALIZE_ALL, StringToWordVector.TAGS_FILTER));
        filter.setDebug(false);
        return filter;
    }

    private void createTrainingSetEvents() throws Exception {
        final String TRAININGSET_DIRECTORY = getFilePath(PROJECT_DATA, "training_set");

        String arffFilePath = getFilePath(TRAININGSET_DIRECTORY, "training_set.arff");
        System.out.println(arffFilePath);
        File arffFile = new File(arffFilePath);
        if (!arffFile.exists()) {
            createTrainingSetArff(TRAININGSET_DIRECTORY, arffFilePath);
        }
    }

    private void createTrainingSetArff(String baseDirectory, String arffFileName) throws Exception {
        Attribute textAttribute = new Attribute("text", (List<String>) null);

        /* defining possible values of nominal class attribute */
        List<String> nominalValues = new ArrayList<String>();
        nominalValues.add("positive");
        nominalValues.add("negative");
        nominalValues.add("neutral");
        Attribute classAttribute = new Attribute("tweet_class", nominalValues);

        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        attributes.add(textAttribute);
        attributes.add(classAttribute);

        /* lets construct our training set with an initial capacity of 10 elements */
        int initialCapacity = 10;
        Instances trainingInstances = new Instances("training_set", attributes, initialCapacity);
        /* last attribute index is the class (it starts from 0 to N - 1) */
        trainingInstances.setClassIndex(trainingInstances.numAttributes() - 1);

        String neutralFilePath = getFilePath(baseDirectory, TR_NEUTRAL);
        String positiveFilePath = getFilePath(baseDirectory, TR_POSITIVE);
        String negativeFilePath = getFilePath(baseDirectory, TR_NEGATIVE);

        addInstancesFromFile(trainingInstances, "neutral", neutralFilePath);
        addInstancesFromFile(trainingInstances, "positive", positiveFilePath);
        addInstancesFromFile(trainingInstances, "negative", negativeFilePath);

        /* store into an arff file */
        ArffSaver saver = new ArffSaver();
        saver.setInstances(trainingInstances);
        saver.setFile(new File(arffFileName));
        saver.writeBatch();
    }

    private void addInstancesFromFile(Instances trainingInstances, String classLabel, String filePath) throws Exception {
        /* loading data from json file and assign a random class value */
        File file = new File(filePath);
        if (!file.exists()) {
            System.out.println("File doesn't exist: " + filePath);
            return;
        }
        int numberOfLines = 0;
        BufferedReader bf = null;
        String newLine = null;
        try {
            bf = new BufferedReader(new FileReader(file));
            // to skip the file header
            bf.readLine();
            while ((newLine = bf.readLine()) != null) {
                if (newLine.isEmpty()) {
                    continue;
                }
                numberOfLines++;
                String[] values = newLine.split(SEPARATOR_REGEX);
                /* get text attribute value */
                String text = StringUtils.unaccent(values[0]);
                text = Utils.quote(text);
                /* create a new instance */
                Instance instance = new DenseInstance(2);
                instance.setValue(trainingInstances.attribute(0), text);
                instance.setValue(trainingInstances.attribute(1), classLabel);
                /* add it to a list of instances */
                trainingInstances.add(instance);
            }
        } finally {
            if (bf != null) {
                bf.close();
            }
        }
        System.out.println(numberOfLines + " tweets readed from " + filePath);
    }

    private String getFilePath(String... subFolders) {
        String fileSeparator = System.getProperty("file.separator");
        String pathToReturn = subFolders[0];
        for (int i = 1; i < subFolders.length; i++) {
            pathToReturn = pathToReturn + fileSeparator + subFolders[i];
        }
        return pathToReturn;
    }

    public static boolean isItalian(String text) {
        LanguageIdentifier identifier = new LanguageIdentifier(text);
        String language = identifier.getLanguage();
        return LANGUAGE.equalsIgnoreCase(language);
    }

    private static Classifier buildClassifier(ClassifierEnum classifierEnum) throws Exception {
        Classifier classifier = null;
        switch (classifierEnum) {
            /* case SVM:
                LibSVM svm = new LibSVM();
                svm.setSVMType(new SelectedTag(LibSVM.SVMTYPE_C_SVC, LibSVM.TAGS_SVMTYPE));
                svm.setKernelType(new SelectedTag(LibSVM.KERNELTYPE_RBF, LibSVM.TAGS_KERNELTYPE));
                svm.setOptions("-G 0.002 -S 0 -K 2 -D 3 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -V".split(" "));
                classifier = svm;
                break;
            case J48:
                J48 j48 = new J48();
                j48.setOptions("-C 0.08000000000000002 -M 2".split(" "));
                classifier = j48;
                break;
            case NAIVE_BAYES:
                classifier = new NaiveBayes();
                break;                */
            case MULTINOMIAL_NAIVE_BAYES:
                classifier = new NaiveBayesMultinomial();
                break;
        }
        return classifier;
    }

    public static void main(String[] args) throws Exception {
        new ClassifierBuilder();
    }

}
