/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.iet.dm.sentiweb.database;

import it.unipi.iet.dm.sentiweb.admin.NewTweetInfo;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author n3b1u
 */
public class Database {

    static String DB_URL = "jdbc:mysql://localhost:3306/TweetDb";
    static String USER = "admin";
    static String PASS = "admin";
    static SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss");

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void insertToDb(String tweet, String polarity) throws SQLException {
        Connection myConn = null;
        PreparedStatement myStmt = null;
        String sql = "INSERT INTO tweets_content(dateTime, tweetContent, polarity) "
                + "VALUES (now(),?,?)";
        try {
            myConn = DriverManager.getConnection(DB_URL, USER, PASS);
            myStmt = myConn.prepareStatement(sql);
            myStmt.setString(1, tweet);
            myStmt.setString(2, polarity);
            myStmt.executeUpdate();
        } finally {
            if (myStmt != null) {
                try {
                    myStmt.close();
                } catch (Exception ex) {

                }
            }
            if (myConn != null) {
                try {
                    myConn.close();
                } catch (Exception ex) {

                }
            }
        }

    }

    public static List<NewTweetInfo> getTodaysTweets() throws SQLException {
        Connection myConn = null;
        PreparedStatement myStmt = null;
        ResultSet rs = null;
        String sql = "SELECT dateTime, tweetContent, polarity FROM TweetDb.tweets_content where date(dateTime) = date(?) order by dateTime desc";
        List<NewTweetInfo> infos = new ArrayList<NewTweetInfo>();
        try {
            myConn = DriverManager.getConnection(DB_URL, USER, PASS);
            myStmt = myConn.prepareStatement(sql);
            myStmt.setDate(1, new java.sql.Date(new java.util.Date().getTime()));
            rs = myStmt.executeQuery();
            while (rs.next()) {
                NewTweetInfo tweetInfo = new NewTweetInfo();
                tweetInfo.setTime(SDF.format(new java.util.Date(rs.getTimestamp(1).getTime())));
                tweetInfo.setText(rs.getString(2));
                tweetInfo.setPredictedClass(rs.getString(3));
                infos.add(tweetInfo);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {

                }
            }
            if (myStmt != null) {
                try {
                    myStmt.close();
                } catch (Exception ex) {

                }
            }
            if (myConn != null) {
                try {
                    myConn.close();
                } catch (Exception ex) {

                }
            }
        }
        return infos;
    }

    public static NewTweetInfo getTweetsByRange(java.util.Date from, java.util.Date to) throws SQLException {
        Connection myConn = null;
        PreparedStatement myStmt = null;
        ResultSet rs = null;
        String sql = "SELECT polarity, count(id) FROM tweets_content where date(dateTime) between date(?) and date(?) group by polarity";
        NewTweetInfo tweetInfo = new NewTweetInfo();
        try {
            myConn = DriverManager.getConnection(DB_URL, USER, PASS);
            myStmt = myConn.prepareStatement(sql);
            myStmt.setDate(1, new java.sql.Date(from.getTime()));
            myStmt.setDate(2, new java.sql.Date(to.getTime()));
            rs = myStmt.executeQuery();
            int total = 0;
            while (rs.next()) {
                String polarity = rs.getString(1);
                Integer count = rs.getInt(2);
                if ("positive".equals(polarity)) {
                    tweetInfo.setPositive(count);
                } else if ("negative".equals(polarity)) {
                    tweetInfo.setNegative(count);
                } else if ("neutral".equals(polarity)) {
                    tweetInfo.setNeutral(count);
                }
                total = total + count;
            }
            tweetInfo.setTotal(total);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {

                }
            }
            if (myStmt != null) {
                try {
                    myStmt.close();
                } catch (Exception ex) {

                }
            }
            if (myConn != null) {
                try {
                    myConn.close();
                } catch (Exception ex) {

                }
            }
        }
        return tweetInfo;
    }
}
