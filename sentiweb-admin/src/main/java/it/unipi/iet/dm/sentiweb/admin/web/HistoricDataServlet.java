/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.iet.dm.sentiweb.admin.web;

import it.unipi.iet.dm.sentiweb.admin.ApplicationHandler;
import it.unipi.iet.dm.sentiweb.admin.NewTweetInfo;
import it.unipi.iet.dm.sentiweb.database.Database;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n3b1u
 */
public class HistoricDataServlet extends HttpServlet {

    
    private SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy"); 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, 2016);
            calendar.set(Calendar.MONTH, 1);
            calendar.set(Calendar.DATE, 1);
            Date from = calendar.getTime();
            Date to = new Date();
            
            String fromStr = request.getParameter("from");
            if (fromStr != null && !fromStr.isEmpty()){
                from = SDF.parse(fromStr);
            }
            String toStr = request.getParameter("to");
            if (toStr != null && !toStr.isEmpty()){
                to = SDF.parse(toStr);
            }
            NewTweetInfo newTweetInfo = Database.getTweetsByRange(from, to);
            request.setAttribute("newTweetInfo", newTweetInfo);
            request.getRequestDispatcher("historic_data.jsp").forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
