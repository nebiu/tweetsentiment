/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.iet.dm.sentiweb.admin.core;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import weka.core.Instance;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 *
 * @author n3b1u
 */
public class CustomStringToWordVector extends StringToWordVector {

    private int columnIndexToClean;

    public CustomStringToWordVector(int columnIndexToClean) {
        this.columnIndexToClean = columnIndexToClean;
    }

    @Override
    public boolean input(Instance instance) throws Exception {

        String text = instance.stringValue(columnIndexToClean);
        text = normalizeText(text);
        instance.setValue(columnIndexToClean, text);
        return super.input(instance);
    }

    private String normalizeText(String text) {
        text = Normalizer.normalize(text, Normalizer.Form.NFD); // translate UTF-8 chars properly
        text = text.toLowerCase().trim();				// only lowercase chars allowed
        
        // get rid of '@mentions'
        Pattern p = Pattern.compile("@\\w+ *");
        Matcher m = p.matcher(text);
        text = m.replaceAll(" ");

        // eliminate URLs
        p = Patterns.WEB_URL;
        m = p.matcher(text);
        text = m.replaceAll(" ");

        // remove phones
        p = Patterns.PHONE;
        m = p.matcher(text);
        text = m.replaceAll(" ");

        // remove email addresses
        p = Patterns.EMAIL_ADDRESS;
        m = p.matcher(text);
        text = m.replaceAll(" ");

        // remove numbers
        p = Pattern.compile("(\\d+[,/%]?\\d*)");
        m = p.matcher(text);
        text = m.replaceAll(" ");

        p = Pattern.compile("[!\\\"#$%&'()*+,-./:;<=>?@[\\\\]^_`{|}~]");
        m = p.matcher(text);
        text = m.replaceAll(" ");

        // perhaps we can replace emoticons by text ???
        // remove # from hashtags
        text = text.replace("#", " ");
                
        // just leave one white space between words
        text = text.replaceAll("[\\s]+", " ");        
        return text.trim();
    }

}
