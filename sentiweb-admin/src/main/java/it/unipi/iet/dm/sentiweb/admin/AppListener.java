
package it.unipi.iet.dm.sentiweb.admin;

import it.unipi.iet.dm.sentiweb.admin.core.ClassifierBuilder;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

/**
 * Web application lifecycle listener.
 *
 * @author n3b1u
 */
public class AppListener implements ServletContextListener {

    public static ClassifierBuilder classifierBuilder = null;
    private TwitterStream twitterStream = null;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        /* create a new classifier builder instance */
        try {
            classifierBuilder = new ClassifierBuilder();
        } catch (Exception ex) {
            System.out.println("Error building the classifier.");
            ex.printStackTrace();
        }

        
        /* load existing today's classified tweets */
        try {
            ApplicationHandler.loadTodaysTweets();
        } catch (Exception ex) {
            System.out.println("Unable to load today's tweets file.");
        }
        /* start receiving new tweets to classify them*/
        twitterStream = new TwitterStreamFactory().getInstance();
        initTweetStreaming();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("contextDestroyed");
        if (twitterStream != null){
            twitterStream.shutdown();
        }
    }

    public void initTweetStreaming() {
        twitterStream.addListener(new StatusListener() {
            public void onStatus(Status status) {
                try {
                    ApplicationHandler.classifyNewTweet(status);
                } catch (Exception ex) {
                    System.out.println("Error on during classification of: " + status.getText());
                    ex.printStackTrace();
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice sdn) {
                System.out.println("onDeletionNotice");
            }

            @Override
            public void onTrackLimitationNotice(int i) {
                System.out.println("onTrackLimitationNotice");
            }

            @Override
            public void onScrubGeo(long l, long l1) {
                System.out.println("onScrubGeo");
            }

            @Override
            public void onStallWarning(StallWarning sw) {
                System.out.println("onStallWarning");
            }

            @Override
            public void onException(Exception excptn) {
                System.out.println("onException");
            }
        });
        FilterQuery tweetFilterQuery = new FilterQuery(); 
        tweetFilterQuery.track(new String[]{"ius soli", "iussoli"}); // OR on keywords
        twitterStream.filter(tweetFilterQuery);
    }
}
