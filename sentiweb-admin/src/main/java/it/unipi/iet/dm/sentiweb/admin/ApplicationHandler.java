
package it.unipi.iet.dm.sentiweb.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unipi.iet.dm.sentiweb.admin.core.ClassifierBuilder;
import it.unipi.iet.dm.sentiweb.database.Database;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javax.websocket.Session;
import twitter4j.Status;


public class ApplicationHandler {

    public static List<NewTweetInfo> newTweetsInfo = new ArrayList<NewTweetInfo>();
    private static SimpleDateFormat SDF_TIME = new SimpleDateFormat("HH:mm:ss");
    private static SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat SDF_TWEET_DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static List<Session> sessions = new ArrayList<Session>();

    public static int total;
    public static int negative;
    public static int positive;
    public static int neutral;

    private static final String PROJECT_DATA;
    private static final String SEPARATOR_REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";

    static {
        PROJECT_DATA = getFilePath(System.getProperty("user.home"), "Documents", "dm_project");
    }

    public static void classifyNewTweet(Status status) throws Exception {
        String text = status.getText();

        if (!ClassifierBuilder.isItalian(text)) {
            System.out.println("Non Italian text: " + text);
            return;
        }

        JsonObject newTweetJSON = new JsonObject();
        newTweetJSON.addProperty("id", status.getId());
        newTweetJSON.addProperty("username", status.getUser().getScreenName());
        newTweetJSON.addProperty("date", SDF_TWEET_DATE.format(status.getCreatedAt()));
        newTweetJSON.addProperty("text", text);

        String time = SDF_TIME.format(status.getCreatedAt());
        String predictedClass = AppListener.classifierBuilder.classifyTweet(text);

        newTweetJSON.addProperty("class", predictedClass);
        

        NewTweetInfo newTweetInfo = new NewTweetInfo();
        newTweetInfo.setText(text);
        newTweetInfo.setPredictedClass(predictedClass);
        newTweetInfo.setTime(time);
        
        
        Database.insertToDb(text, predictedClass);

        newTweetsInfo.add(0, newTweetInfo);
        total++;
        switch (predictedClass) {
            case "positive":
                positive++;
                break;
            case "negative":
                negative++;
                break;
            case "neutral":
                neutral++;
                break;
        }

        newTweetInfo.setPositive(positive);
        newTweetInfo.setNegative(negative);
        newTweetInfo.setNeutral(neutral);
        newTweetInfo.setTotal(total);
        
        //notify all web socket clients
        for (Session session : sessions) {
            try {
                session.getBasicRemote().sendObject(newTweetInfo);
            } catch (Exception ex) {
                System.out.println("An error occurred during communication with " + session.getId());
            }
        }
    }

    public static void registerSession(Session session) {
        sessions.add(session);
    }

    public static void closedSession(Session session) {
        sessions.remove(session);
    }

    public static void loadTodaysTweets() throws Exception {
        newTweetsInfo = Database.getTodaysTweets();
        for(NewTweetInfo tweetInfo: newTweetsInfo){
            String predictedClass = tweetInfo.getPredictedClass();
            total++;
            switch (predictedClass) {
                case "positive":
                    positive++;
                    break;
                case "negative":
                    negative++;
                    break;
                case "neutral":
                    neutral++;
                    break;
            }
        }
    }



    private static String getFilePath(String... subFolders) {
        String fileSeparator = System.getProperty("file.separator");
        String pathToReturn = subFolders[0];
        for (int i = 1; i < subFolders.length; i++) {
            pathToReturn = pathToReturn + fileSeparator + subFolders[i];
        }
        return pathToReturn;
    }

}
