/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unipi.iet.dm.sentiweb.websocket;

import it.unipi.iet.dm.sentiweb.admin.ApplicationHandler;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;


@ServerEndpoint(value = "/endpoint", encoders = IussoliWSEncoder.class)
public class IussoliWSEndPoint {

    @OnOpen
    public void open(final Session session) {
        ApplicationHandler.registerSession(session);
    }

    @OnClose
    public void sessionClosed(Session session) throws Exception {
        ApplicationHandler.closedSession(session);
    }
}
