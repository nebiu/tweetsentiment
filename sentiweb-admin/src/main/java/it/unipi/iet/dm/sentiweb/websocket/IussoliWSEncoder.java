package it.unipi.iet.dm.sentiweb.websocket;

import com.google.gson.Gson;
import it.unipi.iet.dm.sentiweb.admin.NewTweetInfo;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;


public class IussoliWSEncoder implements Encoder.Text<NewTweetInfo> {

    @Override
    public String encode(NewTweetInfo object) throws EncodeException {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

}
