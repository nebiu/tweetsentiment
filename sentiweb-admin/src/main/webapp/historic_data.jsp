<%@page import="it.unipi.iet.dm.sentiweb.admin.NewTweetInfo"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ius soli - Opinion Monitoring</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
        
        <link rel="stylesheet" href="bower_components/jquery-ui/themes/base/jquery-ui.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
                <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
            <script src="bower_components/morris.js/morris.min.js"></script>
            <!-- FastClick -->
            <script src="bower_components/fastclick/lib/fastclick.js"></script>
            <!-- AdminLTE App -->
            <script src="dist/js/adminlte.min.js"></script>
            <!-- Sparkline -->
            <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
            <!-- jvectormap  -->
            <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
            <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
            <!-- SlimScroll -->
            <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
            <!-- ChartJS -->
            <script src="bower_components/chart.js/Chart.js"></script>
            <!-- AdminLTE for demo purposes -->
            <script src="dist/js/demo.js"></script>        
            
              <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Twitter Monitor</b> </span>
             </a>


            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="active treeview menu-open">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="RealTimeServlet"><i class="fa fa-circle-o"></i> Real Time Monitoring</a></li>
                                <li class="active"><a href="HistoricDataServlet"><i class="fa fa-circle-o"></i> Historic Data   </a></li>
                            </ul>
                      </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
            <div>
                <%
                    NewTweetInfo newTweetInfo = (NewTweetInfo) request.getAttribute("newTweetInfo");
                %> 
                <!-- Content Header (Page header) -->
        
                <section class="content-header" >
                    <h1>
                        View Report From: 
                        <div style="display: inline;">
                        <input type="text" id="fromDatePicker">
                        To:
                        <input type="text" id="toDatePicker">
                        <input type="button" id="search" value="Query">
                    </h1>
                    
                    </div>
                  
                </section>
                
                    <script>
            $(function () {
                // Donut Chart
                 $("#fromDatePicker").datepicker( { dateFormat: 'dd/mm/yy' });
                  $("#toDatePicker").datepicker( { dateFormat: 'dd/mm/yy' } );
                  $("#search").click(function () {
                       var url = "<%= request.getContextPath() %>/HistoricDataServlet";
                       var fromDatePicker = $("#fromDatePicker").val();
                       var toDatePicker = $("#toDatePicker").val();
                       url = url + "?from=" + fromDatePicker + "&to=" + toDatePicker;
                       window.location = url;
                  });
                var positive = <%= newTweetInfo.getPositive() * 1.0 / newTweetInfo.getTotal() %> * 100.00;
                var negative = <%= newTweetInfo.getNegative() * 1.0/ newTweetInfo.getTotal() %> * 100.00;
                var neutral = <%= newTweetInfo.getNeutral() * 1.0/ newTweetInfo.getTotal() %> * 100.00;
                
                var donut = new Morris.Donut({
                    element: 'sales-chart',
                    resize: true,
                    colors: ['green', 'orange', 'red'],
                    data: [
                        {label: 'Positive %', value: positive.toFixed(2)},
                        {label: 'Neutral %', value: neutral.toFixed(2)},
                        {label: 'Negative %', value: negative.toFixed(2)}

                    ],
                    hideHover: 'auto'
                });
                donut.redraw();
            });
            
           
            </script>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <!-- Left col -->
                            <div class="col-xs-12">
                                <!-- Custom tabs (Charts with tabs)-->
                                <div class="nav-tabs-custom">
                                    <!-- Tabs within a box -->
                                    <ul class="nav nav-tabs pull-right">
                                        <li class="pull-left header"><i class="fa fa-inbox"></i> Opinion distribution</li>
                                    </ul>
                                    <div class="tab-content no-padding">
                                        <!-- Morris chart - Sales -->
                                        <div class="chart tab-pane active" id="sales-chart" style="position: relative; height: 300px;"></div>
                                    </div>
                                </div>
                                <!-- /.nav-tabs-custom -->
                            </div>
                        </div>                    

                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-lg-3 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <h3><%= newTweetInfo.getTotal()%></h3>

                                        <p>Total tweets</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-twitter"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3><%= newTweetInfo.getPositive()%></h3>

                                        <p>Positive</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-thumbs-o-up"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h3><%= newTweetInfo.getNeutral()%></h3>

                                        <p>Neutral</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-hand-peace-o"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-red">
                                    <div class="inner">
                                        <h3><%= newTweetInfo.getNegative()%></h3>

                                        <p>Negative</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-thumbs-o-down"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- ./col -->
                        </div>
                    </section>
                    <!-- /.content -->
                </div>
            </div>
            <!-- /.content-wrapper -->
        </div>

    </body>
</html>
