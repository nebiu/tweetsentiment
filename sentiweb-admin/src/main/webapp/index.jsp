<%@page import="java.util.List"%>
<%@page import="it.unipi.iet.dm.sentiweb.admin.NewTweetInfo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ius soli - Opinion Monitoring</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- jQuery 3 -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="bower_components/raphael/raphael.min.js"></script>
        <script src="bower_components/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="bower_components/moment/min/moment.min.js"></script>
        <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="dist/js/demo.js"></script>        
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper" >

            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Twitter Monitor</b> </span>
                </a>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="active treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active"><a href="RealTimeServlet"><i class="fa fa-circle-o"></i> Real Time Monitoring</a></li>
                                <li><a href="HistoricDataServlet"><i class="fa fa-circle-o"></i> Historic Data </a></li>
                            </ul>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Real time Sentiment Monitoring - "Ius Soli"
                        <small></small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <%

                            int total = Integer.parseInt(request.getAttribute("total").toString());
                            int negative = Integer.parseInt(request.getAttribute("negative").toString());
                            int neutral = Integer.parseInt(request.getAttribute("neutral").toString());
                            int positive = Integer.parseInt(request.getAttribute("positive").toString());

                        %>               
                        <script>
                            
                        var donut = NaN;
                            
                        $(function () {
                            // Donut Chart

                            var positivePercentage = <%= total%> === 0 ? 0 : <%= positive%> /<%= total%> * 100.00;
                            var negativePercentage = <%= total%> === 0 ? 0 :<%= negative%> /<%= total%> * 100.00;
                            var neutralPercentage = <%= total%> === 0 ? 0 :<%= neutral%> /<%= total%> * 100.00;

                            donut = new Morris.Donut({
                                element: 'tweet-chart',
                                resize: true,
                                colors: ['green', 'orange', 'red'],
                                data: [
                                    {label: 'Positive %', value: positivePercentage.toFixed(2)},
                                    {label: 'Neutral %', value: neutralPercentage.toFixed(2)},
                                    {label: 'Negative %', value: negativePercentage.toFixed(2)}

                                ],
                                hideHover: 'auto'
                            });
                            donut.redraw();

                            connectToTomcatWebSocket();
                        });

                            var wsocket;
                            //ws://localhost:8080/test-web-app/endpoint
                            var applicationContext = "${pageContext.request.contextPath}";
                            var serviceLocation = "ws://" + location.hostname+(location.port ? ':'+location.port: '') + applicationContext + "/endpoint";

                            function connectToTomcatWebSocket() {
                                wsocket = new WebSocket(serviceLocation);
                                wsocket.onmessage = onMessageReceived;
                            }
                            
                            function onMessageReceived(response) {
                                var message = JSON.parse(response.data);
                                document.getElementById("positive").innerHTML = message.positive;
                                document.getElementById("negative").innerHTML = message.negative;
                                document.getElementById("neutral").innerHTML = message.neutral;
                                document.getElementById("total").innerHTML = message.total;
                                
                                var table = document.getElementById("latestTweets");
                                var row = table.insertRow(1);
                                var cell1 = row.insertCell(0);
                                var cell2 = row.insertCell(1);
                                var cell3 = row.insertCell(2);
                                cell1.innerHTML = message.time;
                                cell2.innerHTML = message.text;
                                
                                var classSpan = document.createElement('span')

                                if (message.predictedClass === "positive"){
                                    classSpan.innerHTML = "Positive";
                                    classSpan.className = "label label-success";
                                }else if(message.predictedClass === "negative"){
                                    classSpan.innerHTML = "Negative";
                                    classSpan.className = "label label-danger";
                                }else{
                                    classSpan.innerHTML = "Neutral";
                                    classSpan.className = "label label-warning";
                                }
                                cell3.appendChild(classSpan);
                                
                                var newData = [
                                    {label: 'Positive %', value: (message.positive / message.total * 100.00).toFixed(2)},
                                    {label: 'Neutral %', value: (message.neutral / message.total * 100.00).toFixed(2)},
                                    {label: 'Negative %', value: (message.negative / message.total * 100.00).toFixed(2)}
                                ];
                                
                                donut.setData(newData);
                                donut.redraw();
                            }
                        </script>
                        <!-- Left col -->
                        <div class="col-xs-12">
                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="nav-tabs-custom">
                                <!-- Tabs within a box -->
                                <ul class="nav nav-tabs pull-right">
                                    <li class="pull-left header"><i class="fa fa-inbox"></i> Opinion distribution</li>
                                </ul>
                                <div class="tab-content no-padding">
                                    <!-- Morris chart - Sales -->
                                    <div class="chart tab-pane active" id="tweet-chart" style="position: relative; height: 300px;"></div>
                                </div>
                            </div>
                            <!-- /.nav-tabs-custom -->
                        </div>
                    </div>                    
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><div id="total"><%= total%></div></h3>

                                    <p>Today's tweets</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-twitter"></i>
                                </div>
                                <a href="#latestTweets" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3><div id="positive"><%= positive%></div></h3>

                                    <p>Positive</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </div>
                                <a href="#latestTweets" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><div id="neutral"><%= neutral%></div></h3>

                                    <p>Neutral</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-hand-peace-o"></i>
                                </div>
                                <a href="#latestTweets" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3><div id="negative"><%= negative%></div></h3>

                                    <p>Negative</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-thumbs-o-down"></i>
                                </div>
                                <a href="#latestTweets" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Latest tweets</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" id ="latestTweets">
                                        <tr>
                                            <th>Time</th>
                                            <th>Tweet</th>
                                            <th>Class</th>
                                        </tr>
                                        <%
                                            List<NewTweetInfo> tweets = (List<NewTweetInfo>) request.getAttribute("tweets");
                                        %> 
                                        <%  int i = 0;
                                            for (NewTweetInfo tweet : tweets) {
                                                i++;
                                        %>
                                        <tr>
                                            <td><%= tweet.getTime()%></td>
                                            <td><%= tweet.getText()%></td>
                                            <%
                                                String cssClass = "label-success";
                                                String className = "Positive";

                                                if (tweet.getPredictedClass().equalsIgnoreCase("neutral")) {
                                                    className = "Neutral";
                                                    cssClass = "label-warning";
                                                } else if (tweet.getPredictedClass().equalsIgnoreCase("negative")) {
                                                    className = "Negative";
                                                    cssClass = "label-danger";
                                                }
                                            %>
                                            <td><span class="label <%= cssClass%>"><%= className%></span></td>
                                        </tr>
                                        <%
                                            }
                                        %>                                        
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->

    </body>
</html>
